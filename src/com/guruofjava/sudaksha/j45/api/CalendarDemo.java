package com.guruofjava.sudaksha.j45.api;

import java.util.Calendar;
public class CalendarDemo {
	public static void main(String[] args) {
		Calendar c1 = Calendar.getInstance();
		System.out.println(c1.get(Calendar.YEAR));
		System.out.println(c1.get(Calendar.DAY_OF_MONTH));
		System.out.println(c1.get(Calendar.DAY_OF_WEEK));
		System.out.println(c1.get(Calendar.WEEK_OF_MONTH));
		System.out.println(c1.get(Calendar.DAY_OF_YEAR));
		System.out.println(c1.get(Calendar.MONTH));
		
		c1.set(Calendar.MONTH, 9);
		
		System.out.println(c1);
		
		
	}
	

}
