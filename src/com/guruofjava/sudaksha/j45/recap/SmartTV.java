package com.guruofjava.sudaksha.j45.recap;

public class SmartTV extends Television{
	public final String SOFTWARE_NAME;   
	public SmartTV(){
		  super("Samsung");
		  SOFTWARE_NAME = "SONY";
	  }
	public SmartTV(String software){
		super("Samsung");
		SOFTWARE_NAME = software;
	}

}