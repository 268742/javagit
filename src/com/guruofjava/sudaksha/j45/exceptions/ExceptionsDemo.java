package com.guruofjava.sudaksha.j45.exceptions;

public class ExceptionsDemo {
	
	public static void main(String[] args) {
	
     System.out.println(" Start of MAIN");
     m1();
     System.out.println("End of MAIN");
	}
	public static void m1() {
		System.out.println("Start of M1");
		m2();
		System.out.println("End of M1");  
	}
	public static void m2() {
		System.out.println("Start of M2");
		
		try{

			int i = Integer.parseInt("2687p");
			 System.out.println(Math.sqrt(i));
		}catch(NumberFormatException nfe){
			 System.out.println("Solved NumberFormatException");
			
		}
	     System.out.println("End of M2");
	}
}
