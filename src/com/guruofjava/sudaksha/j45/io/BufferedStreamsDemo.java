package com.guruofjava.sudaksha.j45.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;

public class BufferedStreamsDemo {
	public static void main2(String[] args) {

		BufferedInputStream in = null;
		BufferedOutputStream out = null;

		try {

			in = new BufferedInputStream( new FileInputStream(
					"C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\Pivotal_EDU_SpringBootDeveloper_TrainingBrief.pdf"));
			out = new BufferedOutputStream(new FileOutputStream(
					"C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\Pivotal_EDU_SpringBootDeveloper_TrainingBrief_3.pdf"));

			int i;

			System.out.println(new Date().getTime());
			while ((i = in.read()) != -1) {
				out.write(i);
			}
			System.out.println(new Date().getTime());

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			// if (in != null) {
			if (Objects.requireNonNull(in) != null) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}

			if (Objects.requireNonNull(out) != null) {
				try {
					out.close();
				} catch (IOException ioe) {

				}
			}
		}
	}
	
	public static void main(String[] args) {

		FileInputStream in = null;
		FileOutputStream out = null;

		try {

			in = new FileInputStream(
					"C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\Pivotal_EDU_SpringBootDeveloper_TrainingBrief.pdf");
			out = new FileOutputStream(
					"C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\Pivotal_EDU_SpringBootDeveloper_TrainingBrief_3.pdf");

			int i;

			System.out.println(new Date().getTime());
			while ((i = in.read()) != -1) {
				out.write(i);
			}
			System.out.println(new Date().getTime());

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			// if (in != null) {
			if (Objects.requireNonNull(in) != null) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}

			if (Objects.requireNonNull(out) != null) {
				try {
					out.close();
				} catch (IOException ioe) {

				}
			}
		}
	}
}

