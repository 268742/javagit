package com.guruofjava.sudaksha.j45.io;


import java.io.Serializable;

public class Engine implements Serializable{
	public final transient String FUEL_TYPE;
	private double price;
	public final transient String BRAND;
	public final double CC;
	
	public Engine(final String BRAND, final String FUEL_TYPE, double price, double CC) {
		this.FUEL_TYPE = FUEL_TYPE;
		this.price = price;
		this.BRAND = BRAND;
		this.CC = CC;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Engine[" + BRAND + ", " + FUEL_TYPE + ", " + price + ", " + CC + "]";
	}
}
