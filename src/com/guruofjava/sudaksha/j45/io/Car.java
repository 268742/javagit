package com.guruofjava.sudaksha.j45.io;

import java.io.Serializable;

public class Car implements Serializable{
	public final transient String BRAND;
	public final String MODEL;
	private double price;
	private Engine engine;
	
	public Car(final String BRAND, final String MODEL, double price, Engine engine) {
		this.BRAND = BRAND;
		this.MODEL = MODEL;
		this.price = price;
		this.engine = engine;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	
	@Override
	public String toString() {
		return "Car[" + BRAND + ", " + MODEL + ", " + price + ", " + engine + "]";
	}
	
}
