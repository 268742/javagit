package com.guruofjava.sudaksha.j45.io;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class StreamsDemo{
	public static void main(String[] args){

		FileInputStream in = null;

		try {

			in = new FileInputStream("C:/home/trainee/SatishDemo/temp1.txt");

			int i;

			while ((i = in.read()) != -1) {
				System.out.print((char) i);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}
		}
	}
}

