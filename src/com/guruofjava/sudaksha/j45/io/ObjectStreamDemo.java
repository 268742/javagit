package com.guruofjava.sudaksha.j45.io;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Objects;

//import basics.Marker;

public class ObjectStreamDemo {
public static void main(String[] args) {
		
		ObjectInputStream in = null;
		
		try {
			in = new ObjectInputStream(new FileInputStream("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\cars.txt"));
	
			Object temp = in.readObject();

			if(temp instanceof Car) {
				Car c = (Car)temp;
				
				System.out.println(c);
			}

		}catch(Exception ioe) {
			ioe.printStackTrace();
		}finally {
			if(Objects.nonNull) {
				try {
					in.close();
				}catch(IOException ioe) {
					
				}
			}
		}
	}
	
	public static void main1(String[] args) {
		Car c1 = new Car("Audi", "A4", 6000000, new Engine("Audi", "Petrol", 1500000, 2500));
		
		ObjectOutputStream out = null;
		
		try {
			out = new ObjectOutputStream(new FileOutputStream("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\cars.txt"));
			
			out.writeObject(c1);
		}catch(Exception ioe) {
			ioe.printStackTrace();
		}finally {
			if(Objects.nonNull) {
				try {
					out.close();
				}catch(IOException ioe) {
					
				}
			}
		}
	}


}