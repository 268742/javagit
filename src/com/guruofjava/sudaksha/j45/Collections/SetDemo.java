package com.guruofjava.sudaksha.j45.Collections;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.HashSet;
import java.util.TreeSet;

public class SetDemo {
	public static void main(String[] args) {
		Set s1 = new TreeSet(Arrays.asList(16,5,12,45,47));
		System.out.println(s1);
		System.out.println(s1.first());
		System.out.println(s1.last());
		
		Set s2 = s1.headSet(45);
		
		
	}	
      public static void main4(String[] args) {
    	  Set s1 = new LinkedHashSet();
    	  
    	    s1.add(25);
			s1.add(26);
			s1.add(22);
			//Adding multiple valyes at a time//
			s1.addAll(Arrays.asList(12,45,20));
			System.out.println(s1);
			
			
			
                 //for each//
			for(Object t:s1){
				System.out.println(t);
			}
			System.out.println("---");
			
			Iterator it = s1.iterator();
			while(it.hasNext()){
				System.out.println(it.next());
			}
	}	
	
	
	  public static void main3(String[] args) {
		  Set s1 = new LinkedHashSet();
		  Set s2 = new LinkedHashSet();
			
			s1.add(25);
			s1.add(26);
			s1.add(22);
			
			s2.add(25);
			s2.add(26);
			s2.add(55);
			
			System.out.println(s1);
			System.out.println(s2);
			
			//s1.addAll(s2);
			//s1.removeAll(s2);
			s1.retainAll(s2);
			System.out.println(s1.containsAll(s2));
			System.out.println(s1);
			System.out.println(s2);
	}
	
	public static void main2(String[] args) {
           HashSet s1 = new HashSet();
	
		s1.add(25);
		s1.add(26);
		System.out.println(s1);
		System.out.println(s1.isEmpty());
		System.out.println(s1.size());
		System.out.println(s1.contains(30));
		s1.clear();
		System.out.println(s1.size());
		s1.add(26);
		s1.add(87);
		s1.add(42);
		System.out.println(s1.remove(41));
		s1.remove(42);
		System.out.println(s1);
	}
	
	
	public static void main1(String[] args) {   
		HashSet s1 = new HashSet();
		
		System.out.println(s1);
		s1.add(25);
		System.out.println(s1);
		s1.add(26);
		System.out.println(s1);
	     
		workWithSet(s1);
	}
	
	public static void workWithSet(Set s) {
		
	}
}