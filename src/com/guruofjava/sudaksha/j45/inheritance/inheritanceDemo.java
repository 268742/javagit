package com.guruofjava.sudaksha.j45.inheritance;

public class inheritanceDemo {
	public static void main(String[] args) {
		Animal a1 = new Animal(); 
		 Animal a2 = new Cat(); 
		Cat c1 = new Cat();
		Fish f1 = new Fish();
		a1.move(25);
		c1.move(20);
		c1.hunt();
		a1.WhoAmI();
		a2.WhoAmI();
		c1.WhoAmI();
		a2.move(25);
	
		f1.WhoAmI();
		f1.Eat();
	
	}

}
