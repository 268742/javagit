
public class MARKER {
	private double price;
	public final String brand;
	String color;
	static String category;
	
	public MARKER() {
		/*price = 25.0;
		brand = "Camlin";
		color = "Black";*/
		
		this("Camlin");
	}
	
	public MARKER(String b) {
		this(b, "Black");
		/*price = 25.0;
		brand = b;
		color = "Black";*/
	}
	
	public MARKER(String b, String c) {
		price = 25.0;
		brand = b;
		color = c;
	}
	
	public void write(String s) {
		System.out.println(s);
	}
	
	public void write(int v) {
		System.out.println(v);
	}
	
	public void write(double d) {
		System.out.println(d);
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price)throws IllegalArgumentException {
		if(price > 0) {
			this.price = price;
		}else {
			//System.out.println("Invalid price for Marker");
			IllegalArgumentException iae = new IllegalArgumentException();
			throw iae;
			
		}
	}
}
