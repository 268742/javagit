package multiThreading;

public class ThreadDemo {
	public static void main(String[] args) {
		
		String name = Thread.currentThread().getName();
		
		TestThread t1 = new TestThread();
		TestThread t2 = new TestThread();
		
		
		t1.setName("Black");
		t2.setName("Green");
		t1.start();
		t2.start();
		
		for(int i=1; i<=10;i++){
		System.out.println(name + ": " + i);
		
		}
		
	}

}
