package multiThreading;

public class Producer  extends Thread{
	private Warehouse wp;
	
	public Producer(Warehouse wp) {
		this.wp = wp;
	}

	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		
		for(int i=0; i<10; i++) {
			int temp = (int)(10 + Math.random() * 90);
			wp.setGoods(temp);
			System.out.println(name + ": " + temp);
			
			try {
				Thread.sleep(400);
			}catch(InterruptedException ie) {
				
			}
		}
	}
}
