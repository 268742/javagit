
public class Marker {
	private double price;
	public final String brand;
	String color;
	static String category;
	
	public Marker() {
		/*price = 25.0;
		brand = "Camlin";
		color = "Black";*/
		
		this("Camlin");
	}
	
	public Marker(String b) {
		this(b, "Black");
		/*price = 25.0;
		brand = b;
		color = "Black";*/
	}
	
	public Marker(String b, String c) {
		price = 25.0;
		brand = b;
		color = c;
	}
	
	public void write(String s) {
		System.out.println(s);
	}
	
	public void write(int v) {
		System.out.println(v);
	}
	
	public void write(double d) {
		System.out.println(d);
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		if(price > 0) {
			this.price = price;
		}else {
			System.out.println("Invalid price for Marker");
		}
	}
}
