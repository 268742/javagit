
public class SwapDemo {
	public static void main(String[] args){
		MARKER m1 = new MARKER();
		MARKER m2 = new MARKER();
		System.out.println(((MARKER) m1).getPrice() + " " + m2.getPrice());
		swapPrice(m1 , m2);
		System.out.println(m1.getPrice() + " " + m2.getPrice());
	}
	public static void swapPrice(MARKER x , MARKER y){
		double temp = x.getPrice();
		x.setPrice(y.getPrice());
		y.setPrice(temp);
		
	}
	/*public static void main(String[] args) {
		int a = 5, b = 8;
		System.out.println(a + " " + b);
		
		swap(a, b);
		System.out.println(a + " " + b);
	}*/
public static void swap(int x, int y){
	int temp = x;
	x = y;
	y = temp;
	
}
}
